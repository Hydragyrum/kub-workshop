# Créer son cluster avec Terraform

D'abord, il faut initialiser l'environnement terraform
```bash
cd terraform # (1)
terraform init
```

1. On doit se mettre dans le répertoire `terraform` pour réaliser les étapes

!!! success "Terraform initialisé correctement"
    La commande se termine par **Terraform has been successfully initialized!**


Il faut donner un nom à votre cluster:

```bash
export TF_VAR_OVH_CLOUD_PROJECT_KUBE_NAME=<votre_trigramme> # (1)
```
 
1. 🚨 Mettre un trigramme a minima, voire un chiffre également pour l'unicité des clusters

🚨 Attention à bien respecter les règles de nommage: 

* Pas d'underscore, pas d'espace, pas de majuscule, pas d'accent, pas de caractères spéciaux 
* RIEN !! que des minuscules et - 😊 (regex: '^\[a-z0-9\]([-a-z0-9]*[a-z0-9])?$')

??? info "Variables nécessaires pour initialiser le cluster sur OVH"

    Ensuite, il faut configurer des variables d'environnements pour intéragir avec notre cloud provider:

    ```bash
    export TF_VAR_SERVICE_NAME=""
    export TF_VAR_APPLICATION_KEY=""
    export TF_VAR_APPLICATION_SECRET=""
    export TF_VAR_CONSUMER_KEY=""
    ```

    On est sympa, c'est déjà fait grâce au script exécuté au début du workshop.
    Si vous avez changer de terminal, il faut refaire la commande suivante:
    ```bash
    --8<-- "initscript.sniplet"
    ```


D'autres variables décrites dans `variables.tf` peuvent être surchargées comme par exemple la taille des noeuds ou la localisation du cluster. Mais ...

!!! quote "Horacio González (@LostInBrittany)"
    Le GRA : c'est la vie ! 🧈

donc par défaut nous utilisons le datacenter de *Gravelines*.

??? example "Pour les curieux `variables.tf`"
    ```yaml
    --8<-- "terraform/variables.tf"
    ```

On peut maintenant "planifier" notre déploiement:

```bash
terraform plan -out kub-workshop.plan
```

!!! success "Terraform correctement planifié"
    Aucune erreur n'apparait dans la console.

On peut désormer exécuter le déploiement

```bash
terraform apply kub-workshop.plan
```

??? info "A propos de `kub-workshop.plan`"
    On peut voir qu'un fichier `kub-workshop.plan` a été créé à la racine de notre repo.
    Il s'agit d'un fichier binaire contenant les informations nécessaires à Terraform pour intéragir avec le provider.

L'exécution prend une dizaine de minutes, le temps de prendre un ☕️ car après c'est parti !!

!!! success "Cluster créé"
    **Apply complete! Resources: 2 added, 0 changed, 0 destroyed.**

Maintenant, il faut récupérer notre fichier `kubeconfig` pour intéragir avec notre nouveau cluster:

```bash
terraform output -raw kubeconfig > cluster-ovh-${TF_VAR_OVH_CLOUD_PROJECT_KUBE_NAME}.yml
export KUBECONFIG=$(pwd)/cluster-ovh-${TF_VAR_OVH_CLOUD_PROJECT_KUBE_NAME}.yml
```

Vérifier que la connexion est ok:

```bash
kubectl get nodes -o wide
```

Le résultat ressemble à: (1)
{ .annotate }

1. La version peut différée par rapport à l'exemple

```console
NAME                            STATUS   ROLES    AGE   VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
mvt-snowcamp-pool-node-47dd14   Ready    <none>   20m   v1.27.8   57.128.56.219   <none>        Ubuntu 22.04.3 LTS   5.15.0-91-generic   containerd://1.6.25
```

**💫 Notre cluster est prêt, déployons notre première application [➡️](../initial-stage/README.md)**
